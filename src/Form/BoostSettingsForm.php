<?php

namespace Drupal\boost\Form;

use Drupal\boost\BoostCacheFileInterface;
use Drupal\Component\Plugin\Factory\FactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\system\Plugin\Condition\RequestPath;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Boost settings for this site.
 */
class BoostSettingsForm extends ConfigFormBase {

  /**
   * BoostCache file.
   *
   * @var \Drupal\boost\BoostCacheFileInterface
   */
  protected BoostCacheFileInterface $boostCacheFile;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The request path condition plugin.
   *
   * @var \Drupal\system\Plugin\Condition\RequestPath
   */
  protected RequestPath $condition;

  /**
   * The alias manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Component\Plugin\Factory\FactoryInterface $plugin_factory
   *   The factory interface to create request path condition plugin.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Path alias manager.
   * @param \Drupal\boost\BoostCacheFileInterface $boost_file
   *   The boost file service.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    DateFormatterInterface $date_formatter,
    FactoryInterface $plugin_factory,
    EntityTypeManagerInterface $entity_type_manager,
    BoostCacheFileInterface $boost_file,
  ) {
    $this->configFactory = $config_factory;
    $this->dateFormatter = $date_formatter;
    $this->condition = $plugin_factory->createInstance('request_path');
    $this->entityTypeManager = $entity_type_manager;
    $this->boostCacheFile = $boost_file;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('date.formatter'),
      $container->get('plugin.manager.condition'),
      $container->get('entity_type.manager'),
      $container->get('boost.file')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'boost_boost_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['boost.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('boost.settings');
    $config_performance = $this->config('system.performance');
    $cacheability_pages_open = FALSE;
    $path_entity_open = FALSE;
    $xmlsitemap_crawl_open = FALSE;

    // Copied from core/modules/system/src/Form/PerformanceForm.php.
    $period = [0, 60, 180, 300, 600, 900, 1800, 2700, 3600, 10800, 21600, 32400, 43200, 86400];
    $period = array_map([$this->dateFormatter, 'formatInterval'], array_combine($period, $period));
    $period[0] = '<' . $this->t('no caching') . '>';

    $system_cache_page_max_age = $period[$config_performance->get('cache.page.max_age')];

    if ($cacheability_pages = $config->get('cacheability_pages')) {
      if (trim($cacheability_pages['pages']) != "") {
        $cacheability_pages_open = TRUE;
      }
      $this->condition->setConfiguration($cacheability_pages);
    }

    $pe_set_string = '';
    if ($pe_set = $config->get('path_entity_set')) {
      foreach ($pe_set as $entity_type => $bundles) {
        if ($bundles == []) {
          $pe_set_string .= $entity_type . "\n";
        }
        else {
          $pe_set_string .= $entity_type . ': ' . implode(', ', $bundles) . "\n";
        }
      }
    }
    if (trim($pe_set_string) != '') {
      $path_entity_open = TRUE;
    }

    $xmlsitemap_crawl_string = '';
    if ($xmlsitemap_crawl_array = $config->get('xmlsitemap_crawl')) {
      foreach ($xmlsitemap_crawl_array as $xmlsitemap_crawl) {
        $xmlsitemap_crawl_string .= $xmlsitemap_crawl . "\n";
      }
    }
    if (trim($xmlsitemap_crawl_string) != '') {
      $xmlsitemap_crawl_open = TRUE;
    }

    $form['boost_main'] = [
      '#type' => 'details',
      '#title' => $this->t('Boost Main Settings'),
      '#open' => TRUE,
    ];
    $form['boost_main']['cron_purge_maximum_age'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Cron purge (delete) cached files: cache maximum age'),
      '#default_value' => $config->get('cron_purge_maximum_age'),
      '#description' => $this->t('If enabled on cron a purge will we done based on file modification date. This follows the system config <a href=":performance_site">":performance_title" > ":performance_max_age" (Current value: :period_current)</a>. As an alternative you can directly use drush command "boost:purge-max-age".', [
        ':performance_title' => $this->t('Performance'),
        ':performance_site' => Url::fromRoute('system.performance_settings')->toString(),
        ':performance_max_age' => $this->t('Browser and proxy cache maximum age'),
        ':period_current' => $system_cache_page_max_age,
      ]),
      '#disabled' => FALSE,
    ];
    $form['boost_main']['exec'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('User php exec/shell_exec with os commands "find" and "rm".'),
      '#default_value' => $config->get('exec'),
      '#description' => $this->t("Warning: There is no UI feedback. Check cli commands and file system if this is working for you. But if enabled and it's working the purge (delete) process of cache files is extreme fast."),
      '#disabled' => FALSE,
    ];

    $form['boost_cacheability'] = [
      '#type' => 'details',
      '#title' => $this->t('Boost Path Cacheability Settings'),
      '#open' => $cacheability_pages_open,
    ];
    $form['boost_cacheability'] += $this->condition->buildConfigurationForm($form['boost_cacheability'], $form_state);
    $form['boost_cacheability']['negate']['#type'] = 'radios';
    $form['boost_cacheability']['negate']['#default_value'] = (int) $form['boost_cacheability']['negate']['#default_value'];
    $form['boost_cacheability']['negate']['#title_display'] = 'invisible';
    $form['boost_cacheability']['negate']['#options'] = [
      $this->t('Show for the listed pages'),
      $this->t('Hide for the listed pages'),
    ];
    $form['generate'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings for Boost generate strategies:'),
      '#open' => TRUE,
      '#description' => $this->t('"Generate" default settings. Settings needed to be saved before using buttons below.'),
      '#weight' => 5,
    ];
    $form['generate']['path_entity'] = [
      '#type' => 'details',
      '#title' => $this->t('Entity path strategy'),
      '#open' => $path_entity_open,
      '#description' => $this->t('Use entity paths or alias of selected entities. See drush commands for more possibilities.'),
      '#weight' => 5,
    ];
    $form['generate']['path_entity']['path_entity_set'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Entity path strategy (default set)'),
      '#default_value' => $pe_set_string,
      '#description' => $this->t('One entity type per line, limit bundles via colon and commaty. e.g. "node: article, page'),
    ];
    $form['generate']['xmlsitemap_crawl'] = [
      '#type' => 'details',
      '#title' => $this->t('XML Sitemap (crawl) strategy'),
      '#open' => $xmlsitemap_crawl_open,
      '#description' => $this->t("Crawls every xml sitemap(s) via http request and depends on their availablilty and actuality."),
      '#weight' => 5,
    ];
    $form['generate']['xmlsitemap_crawl']['xmlsitemap_crawl_paths'] = [
      '#type' => 'textarea',
      '#title' => $this->t('xml sitemap paths to crawl and process'),
      '#default_value' => $xmlsitemap_crawl_string,
      '#description' => $this->t('One sitemap per line. Keep aware of splitting huge lists and different language versions.'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save settings above'),
      '#weight' => 6
    ];

    $form['boost_manage_cache'] = [
      '#type' => 'details',
      '#title' => $this->t('Cache Management'),
      '#open' => TRUE,
      '#weight' => 12,
    ];
    $form['boost_manage_cache']['count'] = [
      '#type' => 'submit',
      '#value' => $this->t('Count boost cache files.'),
      '#submit' => ['::countBoostCachedFiles'],
    ];
    $form['boost_manage_cache']['purge'] = [
      '#type' => 'submit',
      '#value' => $this->t('Purge (delete) all cache files.'),
      '#submit' => ['::purgeBoostCache'],
    ];
    $form['boost_manage_cache']['generate'] = [
      '#type' => 'details',
      '#title' => $this->t('Generate boost files ("cache warming"):'),
      '#open' => TRUE,
      '#description' => $this->t('Generate" UI buttons. Related settings above needs to be saved before to activate UI. Usage of Drush is recommend especially on huge content sites.'),
    ];
    if ($xmlsitemap_crawl_open) {
      $form['boost_manage_cache']['generate']['xmlsitemap_raw'] = [
        '#type' => 'submit',
        '#value' => $this->t('Generate xmlsitemap (crawl) paths'),
        '#submit' => ['::generateBoostCacheXmlSitemapCrawl'],
      ];
    }
    if ($path_entity_open) {
      $form['boost_manage_cache']['generate']['path_entity'] = [
        '#type' => 'submit',
        '#value' => $this->t('Generate entity paths default set'),
        '#submit' => ['::generateBoostCachePathEntity'],
      ];
    }
    $form['boost_manage_cache']['generate']['alias_all'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate all aliases (not recommended)'),
      '#submit' => ['::generateBoostCacheAliasAll'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_values = $form_state->getValues();
    $this->condition->submitConfigurationForm($form['boost_cacheability'], $form_state);

    $pe_set = [];
    $pe_set_raw = $form_values['path_entity_set'];
    $pe_set_lines = explode("\n", trim(str_replace(["\r\n", "\n\r", "\r"], "\n", trim($pe_set_raw))));
    foreach ($pe_set_lines as $pe_set_line) {
      $pe_set_line_colon = array_map('trim', explode(':', $pe_set_line));
      if (isset($pe_set_line_colon[1])) {
        $pe_set_line_bundles = explode(',', $pe_set_line_colon[1]);
        $pe_set[$pe_set_line_colon[0]] = array_map('trim', $pe_set_line_bundles);
      }
      else {
        $pe_set[$pe_set_line_colon[0]] = [];
      }
    }

    $xsc_config_array = [];
    $xsc_raw = $form_values['xmlsitemap_crawl_paths'];
    $xsc = explode("\n", trim(str_replace(["\r\n", "\n\r", "\r"], "\n", trim($xsc_raw))));
    foreach ($xsc as $xsc_check_raw) {
      $xsc_check = trim($xsc_check_raw);
      if ($xsc_check != '' && $xsc_check != '/') {
        $xsc_check_first = mb_substr($xsc_check, 0, 1);
        if ($xsc_check_first != '/') {
          $xsc_config_array[] = '/' . $xsc_check;
        }
        else {
          $xsc_config_array[] = $xsc_check;
        }
      }
    }

    $this->config('boost.settings')
      ->set('cron_purge_maximum_age', $form_values['cron_purge_maximum_age'])
      ->set('cacheability_pages', $this->condition->getConfiguration())
      ->set('exec', $form_values['exec'])
      ->set('path_entity_set', $pe_set)
      ->set('xmlsitemap_crawl', $xsc_config_array)
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Count boost caches files.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Formstate object.
   */
  public function countBoostCachedFiles(array &$form, FormStateInterface $form_state) {
    $files = $this->boostCacheFile->getCachedFiles();
    $message = $this->t('@count cache files found.', ['@count' => count($files)]);
    \Drupal::messenger()->addMessage($message);
  }

  /**
   * Purge boost cache files.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Formstate object.
   */
  public function purgeBoostCache(array &$form, FormStateInterface $form_state) {
    \Drupal::service('boost.purge')->purgeCacheFiles();
  }

  /**
   * Generate 'xmlsitemap raw' Cache Files.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Formstate object.
   */
  public function generateBoostCacheXmlSitemapCrawl(array &$form, FormStateInterface $form_state) {
    $path_arrays = \Drupal::service('boost.generate')->getXmlSitemapCrawlPaths();
    \Drupal::service('boost.generate')->generateCacheFiles($path_arrays);
  }

  /**
   * Generate 'path_entity' Cache Files.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Formstate object.
   */
  public function generateBoostCachePathEntity(array &$form, FormStateInterface $form_state) {
    $path_arrays = \Drupal::service('boost.generate')->getEntityPaths();
    \Drupal::service('boost.generate')->generateCacheFiles($path_arrays);
  }

  /**
   * Generate 'alias_all' Cache Files .
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Formstate object.
   */
  public function generateBoostCacheAliasAll(array &$form, FormStateInterface $form_state) {
    $path_arrays = \Drupal::service('boost.generate')->getAliasesAllPaths();
    \Drupal::service('boost.generate')->generateCacheFiles($path_arrays);
  }

}
