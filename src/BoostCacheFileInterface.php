<?php

declare(strict_types=1);

namespace Drupal\boost;

/**
 * BoostCache interface.
 */
interface BoostCacheFileInterface {

  /**
   * Default file extension.
   */
  const DEFAULT_EXTENSION = 'html';

  /**
   * Default file scheme.
   */
  const DEFAULT_SCHEME = 'public://';

  /**
   * Default directory.
   */
  const DEFAULT_DIRECTORY = 'boost';

  /**
   * Default mode for new directories.
   */
  const CHMOD_DIRECTORY = 0775;

  /**
   * Gets the file contents by URI.
   *
   * @param string $uri
   *   The URI string.
   *
   * @return string
   *   Returns the requested content from URI.
   */
  public function load(string $uri): string;

  /**
   * Creates a new cache file.
   *
   * @param string $uri
   *   The URI string.
   * @param string $content
   *   The page content as string.
   */
  public function save(string $uri, string $content);

  /**
   * Overrides the existing cache file.
   *
   * @param string $uri
   *   The URI string.
   * @param string $content
   *   The page content as string.
   */
  public function modify(string $uri, string $content);

  /**
   * Deletes the cache file by URI.
   *
   * @param string $uri
   *   The URI string.
   *
   * @return bool
   *   Returns true if the file was deleted.
   */
  public function delete(string $uri): bool;

  /**
   * Retrieves the current file URI.
   *
   * @param string|null $path
   *   The path for the cached file.
   * @param string|null $dir
   *   The directory for the cached file.
   * @param string|null $extension
   *   The extension for the cached file.
   * @param string|null $scheme
   *   The file scheme.
   *
   * @return string
   *   Returns the URI as string.
   */
  public function getUri(string $path = NULL, string $dir = NULL, string $extension = NULL, string $scheme = NULL): string;

  /**
   * Retrieves cached files.
   *
   * @return array
   *   Returns files as array.
   */
  public function getCachedFiles(): array;

  /**
   * Finds all files that match a given mask in a given directory.
   *
   * Directories and files beginning with a dot are excluded; this prevents
   * hidden files and directories (such as SVN working directories) from being
   * scanned. Use the umask option to skip configuration directories to
   * eliminate the possibility of accidentally exposing configuration
   * information. Also, you can use the base directory, recurse, and min_depth
   * options to improve performance by limiting how much of the filesystem has
   * to be traversed.
   *
   * @param string $dir
   *   The base directory or URI to scan, without trailing slash.
   * @param string $mask
   *   The preg_match() regular expression for files to be included.
   * @param array $options
   *   An associative array of additional options, with the following elements:
   *   - 'nomask': The preg_match() regular expression for files to be excluded.
   *     Defaults to the 'file_scan_ignore_directories' setting.
   *   - 'callback': The callback function to call for each match. There is no
   *     default callback.
   *   - 'recurse': When TRUE, the directory scan will recurse the entire tree
   *     starting at the provided directory. Defaults to TRUE.
   *   - 'key': The key to be used for the returned associative array of files.
   *     Possible values are 'uri', for the file's URI; 'filename', for the
   *     basename of the file; and 'name' for the name of the file without the
   *     extension. Defaults to 'uri'.
   *   - 'min_depth': Minimum depth of directories to return files from.
   *     Defaults to 0.
   *
   * @return array
   *   An associative array (keyed on the chosen key) of objects with 'uri',
   *   'filename', and 'name' properties corresponding to the matched files.
   *
   * @throws \Drupal\Core\File\Exception\NotRegularDirectoryException
   *   If the directory does not exist.
   */
  public function scanDirectory($dir, $mask, array $options = []);

}
