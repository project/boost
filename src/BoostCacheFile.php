<?php

declare(strict_types=1);

namespace Drupal\boost;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\Exception\NotRegularDirectoryException;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class for BoostCache file operations.
 */
class BoostCacheFile implements BoostCacheFileInterface {

  /**
   * FileSystem to manage boost cache files.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $request;

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * BoostCacheFile constructor.
   *
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The current request.
   */
  public function __construct(StreamWrapperManagerInterface $stream_wrapper_manager, FileSystemInterface $file_system, RequestStack $request_stack) {
    $this->streamWrapperManager = $stream_wrapper_manager;
    $this->fileSystem = $file_system;
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public function load(string $uri): string {
    return file_get_contents($uri);
  }

  /**
   * {@inheritdoc}
   */
  public function save(string $uri, string $content) {
    $this->directory($uri);
    $this->modify($uri, $content);
  }

  /**
   * {@inheritdoc}
   */
  public function modify(string $uri, string $content) {
    $this->fileSystem->saveData($content, $uri, FileSystemInterface::EXISTS_REPLACE);
  }

  /**
   * {@inheritdoc}
   */
  public function delete(string $uri): bool {
    return $this->fileSystem->unlink($uri);
  }

  /**
   * {@inheritdoc}
   */
  public function getUri(string $path = NULL, string $dir = NULL, string $extension = NULL, string $scheme = NULL): string {
    $scheme = $scheme ?? BoostCacheFileInterface::DEFAULT_SCHEME;
    $dir = $dir ?? BoostCacheFileInterface::DEFAULT_DIRECTORY;
    if (!$path) {
      $path = $this->request->getRequestUri();
    }
    $extension = $extension ?? BoostCacheFileInterface::DEFAULT_EXTENSION;
    return $scheme . $dir . $path . '.' . $extension;
  }

  /**
   * {@inheritdoc}
   */
  public function getCachedFiles(array $options = []): array {
    $files = [];
    $scheme = $scheme ?? BoostCacheFileInterface::DEFAULT_SCHEME;
    $dir = $dir ?? BoostCacheFileInterface::DEFAULT_DIRECTORY;
    $cache_folder = $scheme . $dir;
    try {
      $files = $this->scanDirectory($cache_folder, '/.*/', $options);
    }
    catch (NotRegularDirectoryException $e) {
      \Drupal::messenger()->addWarning($e->getMessage());
      return $files;
    }
    return $files;
  }

  /**
   * {@inheritdoc}
   */
  private function directory(string $uri) {
    $dir = $this->fileSystem->dirname($uri);
    if (!file_exists($dir)) {
      $this->fileSystem->mkdir($dir, BoostCacheFileInterface::CHMOD_DIRECTORY, TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function scanDirectory($dir, $mask, array $options = []) {
    // Merge in defaults.
    $options += [
      'callback' => 0,
      'recurse' => TRUE,
      'key' => 'uri',
      'min_depth' => 0,
    ];
    $dir = $this->streamWrapperManager->normalizeUri($dir);
    if (!is_dir($dir)) {
      throw new NotRegularDirectoryException("$dir is not a directory.");
    }
    // Allow directories specified in settings.php to be ignored. You can use
    // this to not check for files in common special-purpose directories. For
    // example, node_modules and bower_components. Ignoring irrelevant
    // directories is a performance boost.
    if (!isset($options['nomask'])) {
      // $ignore_directories =
      // $this->settings->get('file_scan_ignore_directories', []);
      $ignore_directories = [];
      array_walk($ignore_directories, function (&$value) {
        $value = preg_quote($value, '/');
      });
      $options['nomask'] = '/^' . implode('|', $ignore_directories) . '$/';
    }
    $options['key'] = in_array($options['key'], ['uri', 'filename', 'name']) ? $options['key'] : 'uri';
    return $this->doScanDirectory($dir, $mask, $options);
  }

  /**
   * Internal function to handle directory scanning with recursion.
   *
   * @param string $dir
   *   The base directory or URI to scan, without trailing slash.
   * @param string $mask
   *   The preg_match() regular expression for files to be included.
   * @param array $options
   *   The options as per ::scanDirectory().
   * @param int $depth
   *   The current depth of recursion.
   *
   * @return array
   *   An associative array as per ::scanDirectory().
   *
   * @throws \Drupal\Core\File\Exception\NotRegularDirectoryException
   *   If the directory does not exist.
   *
   * @see \Drupal\Core\File\FileSystemInterface::scanDirectory()
   */
  protected function doScanDirectory($dir, $mask, array $options = [], $depth = 0) {
    $files = [];
    $exclude = ['.', '..'];
    // Avoid warnings when opendir does not have the permissions to open a
    // directory.
    if ($handle = @opendir($dir)) {
      while (FALSE !== ($filename = readdir($handle))) {

        if (!in_array($filename, $exclude)) {
          if (substr($dir, -1) == '/') {
            $uri = "{$dir}{$filename}";
          }
          else {
            $uri = "{$dir}/{$filename}";
          }
          if ($options['recurse'] && is_dir($uri)) {

            // Give priority to files in this folder by merging them in after
            // any subdirectory files.
            $files = array_merge($this
              ->doScanDirectory($uri, $mask, $options, $depth + 1), $files);
          }
          elseif ($depth >= $options['min_depth'] && preg_match($mask, $filename)) {

            // Always use this match over anything already set in $files with
            // the same $options['key'].
            $file = new \stdClass();
            $file->uri = $uri;
            $file->filename = $filename;
            $file->name = pathinfo($filename, PATHINFO_FILENAME);
            if (isset($options['filemtime']) && $options['filemtime'] == TRUE) {
              $file->filemtime = filemtime($uri);
            }
            $key = $options['key'];
            $files[$file->{$key}] = $file;
            if ($options['callback']) {
              $options['callback']($uri);
            }
          }
        }
      }
      closedir($handle);
    }
    else {
      $this->logger
        ->error('@dir can not be opened', [
          '@dir' => $dir,
        ]);
    }
    return $files;
  }

}
