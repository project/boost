<?php

declare(strict_types=1);

namespace Drupal\boost;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * The BoostCache service.
 */
class BoostCache implements BoostCacheInterface {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelFactoryInterface $loggerFactory;

  /**
   * BoostCache file.
   *
   * @var BoostCacheFileInterface
   */
  protected BoostCacheFileInterface $boostCacheFile;

  /**
   * BoostCache service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The Logger channel.
   * @param BoostCacheFileInterface $boost_file
   *   The boost file service.
   */
  public function __construct(
      ConfigFactoryInterface $config_factory,
      LoggerChannelFactoryInterface $logger_factory,
      BoostCacheFileInterface $boost_file
    ) {
    $this->configFactory = $config_factory;
    $this->loggerFactory = $logger_factory;
    $this->boostCacheFile = $boost_file;
  }

  /**
   * {@inheritdoc}
   */
  public function index(string $response) {
    $uri = $this->boostCacheFile->getUri();
    if (empty($response)) {
      $this->loggerFactory->get('boost')->error('Index route @uri can not be done.',
        [
          '@uri' => $uri,
        ]
      );
      return;
    }
    $this->boostCacheFile->save($uri, $response);
  }

  /**
   * {@inheritdoc}
   */
  public function retrieve(): string {
    $uri = $this->boostCacheFile->getUri();
    if (!file_exists($uri)) {
      $this->loggerFactory->get('boost')->error('Retrieve route @uri can not be done.',
        [
          '@uri' => $uri,
        ]
      );
      return '';
    }
    return $this->boostCacheFile->load($uri);
  }

  /**
   * {@inheritdoc}
   */
  public function delete(): bool {
    $uri = $this->boostCacheFile->getUri();
    if (!file_exists($uri)) {
      $this->loggerFactory->get('boost')->error('Delete route @uri can not be done.',
        [
          '@uri' => $uri,
        ]
      );
      return FALSE;
    }
    return $this->boostCacheFile->delete($uri);
  }

}
