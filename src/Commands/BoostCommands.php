<?php

namespace Drupal\boost\Commands;

use Drupal\boost\BoostCacheFileInterface;
use Drupal\boost\BoostCacheGenerate;
use Drupal\boost\BoostCachePurge;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drush\Commands\DrushCommands;

/**
 * Drush 9 commands for Boost module.
 */
class BoostCommands extends DrushCommands {
  use StringTranslationTrait;

  /**
   * The Boost settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $boostSettings;

  /**
   * BoostCache file.
   *
   * @var \Drupal\boost\BoostCacheFileInterface
   */
  protected BoostCacheFileInterface $boostCacheFile;

  /**
   * The generate cache files batch helper.
   *
   * @var \Drupal\boost\BoostCacheGenerate
   */
  protected BoostCacheGenerate $boostCacheGenerate;

  /**
   * The purge cache files batch helper.
   *
   * @var \Drupal\boost\BoostCachePurge
   */
  protected BoostCachePurge $boostCachePurge;

  /**
   * BoostCommands constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory to get boost settings.
   * @param \Drupal\boost\BoostCacheFileInterface $boost_file
   *   The boost file service.
   * @param \Drupal\boost\BoostCacheGenerate $boost_CacheGenerate
   *   The BoostCacheGenerate service.
   * @param \Drupal\boost\BoostCachePurge $boost_CachePurge
   *   The BoostCachePurge service.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    BoostCacheFileInterface $boost_file,
    BoostCacheGenerate $boost_CacheGenerate,
    BoostCachePurge $boost_CachePurge,
  ) {
    parent::__construct();
    $this->boostSettings = $configFactory->get('boost.settings');
    $this->boostCacheFile = $boost_file;
    $this->boostCacheGenerate = $boost_CacheGenerate;
    $this->boostCachePurge = $boost_CachePurge;
  }

  /**
   * Boost count cache files.
   *
   * @command boost:count-cache-files
   *
   * @aliases boost-ccf
   */
  public function countCachedFiles() {
    $files = $this->boostCacheFile->getCachedFiles();
    $message = \Drupal::translation()->formatPlural(
      count($files),
      'No boost cache file found.', '@count boost cache files found.'
    );
    \Drupal::messenger()->addMessage($message);
  }

  /**
   * Boost generate cache files: entity-paths.
   *
   * @command boost:generate-entity-paths
   *
   * @aliases boost-gep
   */
  public function generateCacheFilesEntityPaths() {
    if ($this->hostCheck()) {
      $path_arrays = $this->boostCacheGenerate->getEntityPaths();
      $this->boostCacheGenerate->generateCacheFiles($path_arrays);
      drush_backend_batch_process();
    }
  }

  /**
   * Boost generate cache files: xmlsitemap-crawl.
   *
   * @command boost:generate-xmlsitemap-crawl
   *
   * @aliases boost-gxc
   */
  public function generateCacheFilesXmlSitemapCrawlPaths() {
    if ($this->hostCheck()) {
      $path_arrays = $this->boostCacheGenerate->getXmlSitemapCrawlPaths();
      $this->boostCacheGenerate->generateCacheFiles($path_arrays);
      drush_backend_batch_process();
    }
  }

  /**
   * Boost generate cache files: aliases-all.
   *
   * @command boost:generate-aliases-all
   *
   * @aliases boost-gaa
   */
  public function generateCacheFilesAliasAllPaths() {
    if ($this->hostCheck()) {
      $path_arrays = $this->boostCacheGenerate->getAliasesAllPaths();
      $this->boostCacheGenerate->generateCacheFiles($path_arrays);
      drush_backend_batch_process();
    }
  }

  /**
   * Purges (delete) all boost cache files.
   *
   * @command boost:purge
   *
   * @aliases boost-purge
   */
  public function purgeCacheFiles($batch_success = FALSE) {
    $batch_success = $this->boostCachePurge->purgeCacheFiles();
    if ($batch_success) {
      drush_backend_batch_process();
    }
  }

  /**
   * Purges (delete) boost cache files (system: page cache maximum age).
   *
   * @command boost:purge-max-age
   *
   * @aliases boost-purge-max-age
   */
  public function purgeMaxAgeCacheFiles($batch_success = FALSE) {
    $batch_success = $this->boostCachePurge->purgeCacheFiles('max_age');
    if ($batch_success) {
      drush_backend_batch_process();
    }
  }

  /**
   * Checks that the site URI is set.
   *
   * @return bool
   *   TRUE if the URI is set, otherwise FALSE.
   */
  public function hostCheck() {
    $host = \Drupal::request()->getHost();
    // Check if the host name is configured.
    if ($host == 'default') {
      throw new \Exception('Site URI not specified, use --uri.');
      return FALSE;
    }
    return TRUE;
  }

}
