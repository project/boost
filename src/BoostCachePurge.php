<?php

declare(strict_types=1);

namespace Drupal\boost;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Boost cache purge batch process.
 */
class BoostCachePurge {
  use DependencySerializationTrait;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * FileSystem object.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * BoostCache file.
   *
   * @var \Drupal\boost\BoostCacheFileInterface
   */
  protected BoostCacheFileInterface $boostCacheFile;

  /**
   * Constructs a configuration object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory to get boost settings.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\boost\BoostCacheFileInterface $boost_file
   *   The boost file service.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(
      ConfigFactoryInterface $config_factory,
      TimeInterface $time,
      FileSystemInterface $file_system,
      BoostCacheFileInterface $boost_file
    ) {
    $this->config = $config_factory;
    $this->time = $time;
    $this->fileSystem = $file_system;
    $this->boostCacheFile = $boost_file;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('datetime.time'),
      $container->get('file_system'),
      $container->get('boost_file')
    );
  }

  /**
   * Purge boost cache files.
   */
  public function purgeCacheFiles($opt = 'all') {
    // Define a feedback boolean for drush;.
    $batch_success = FALSE;
    $config = $this->config->get('boost.settings');
    $exec = $config->get('exec') ?? 0;

    $max_age = 0;
    if ($opt == 'max_age') {
      $max_age = $this->config->get('system.performance')->get('cache.page.max_age');
    }

    if ($exec == 0) {
      $files = [];
      if ($opt == 'max_age') {
        if ($max_age == 0) {
          $files = $files = $this->boostCacheFile->getCachedFiles();
        }
        else {
          $options = ['filemtime' => TRUE];
          $files_all = $this->boostCacheFile->getCachedFiles($options);
          $request_time = $this->time->getCurrentTime();
          $time_limit = $request_time - $max_age;
          foreach ($files_all as $file_uri => $file) {

            $filemtime = $file->filemtime;
            if ($filemtime < $time_limit) {
              $files[$file_uri] = $file;
            }
          }
        }
      }
      else {
        $files = $this->boostCacheFile->getCachedFiles();
      }

      $files_count = count($files);
      if ($files_count == 0) {
        \Drupal::messenger()->addMessage('Boost: No files found to delete');
        return $batch_success;
      }
      else {
        \Drupal::messenger()->addMessage('Boost: files to delete: ' . $files_count);
      }

      $config = $this->config->get('boost.settings');
      $chunk_size = $config->get('chunk_purge') ?? 30;

      $batch_builder = (new BatchBuilder())
        ->setTitle(t('Processing Purge of Boost Cache Files Batch'))
        ->setFinishCallback([$this, 'batchFinished'])
        ->setInitMessage(t('Batch is starting'))
        ->setProgressMessage(t('Processed @current out of @total.'))
        ->setErrorMessage(t('Batch has encountered an error'));

      foreach (array_chunk($files, $chunk_size) as $chunk) {
        $batch_builder->addOperation([$this, 'batchProcess'], [$chunk]);
      }
      $batch_success = TRUE;
      batch_set($batch_builder->toArray());
    }
    else {
      $scheme = $scheme ?? BoostCacheFileInterface::DEFAULT_SCHEME;
      $dir = $dir ?? BoostCacheFileInterface::DEFAULT_DIRECTORY;
      $cache_folder_raw = $this->fileSystem->realpath($scheme . $dir);
      $cache_folder = escapeshellcmd($cache_folder_raw);
      echo $shell_output;
      if ($max_age == 0) {
        $shell_command = 'rm -Rf ' . $cache_folder . '/*';
      }
      else {
        $mmin_raw = $max_age / 60;
        // Just in case the value list change or there is an override:
        $mmin = round($mmin_raw);
        $shell_command = 'find ' . $cache_folder . ' -maxdepth 12 ';
        $shell_command .= '-type f -mmin +' . $mmin . ' | xargs rm -rf';
      }
      exec($shell_command);
      \Drupal::messenger()->addMessage('Boost: Executed shell command for purging.');
    }
    return $batch_success;
  }

  /**
   * Purges files in batches.
   */
  public static function batchProcess($files, &$context) {
    $context['message'] = 'Purging ' . count($files) . ' items';
    foreach ($files as $file) {
      \Drupal::service('file_system')->unlink($file->uri);
      $context['results'][] = $file->name;
    }
  }

  /**
   * Batch process callback after completion.
   *
   * @param bool $success
   *   TRUE if batch successfully completed.
   * @param array $results
   *   Batch results.
   * @param array $operations
   *   Operations.
   */
  public function batchFinished($success = FALSE, array $results = [], array $operations = []) {
    $message = \Drupal::translation()->translate('Finished with an error.');
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One item cleared.', '@count items purged.'
      );
    }
    \Drupal::messenger()->addMessage($message);
  }

}
