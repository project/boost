<?php

declare(strict_types=1);

namespace Drupal\boost;

use Drupal\boost\BoostCacheFileInterface;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Utility\Timer;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\Utils;

/**
 * Boost cache generate batch process.
 */
class BoostCacheGenerate {
  use DependencySerializationTrait;

  /**
   * The alias manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * BoostCache file.
   *
   * @var \Drupal\boost\BoostCacheFileInterface
   */
  protected $boostCacheFile;

  /**
   * Constructs a configuration object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory to get boost settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Path alias manager.
   * @param \Drupal\boost\BoostCacheFileInterface $boost_file
   *   The boost file service.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(
      ConfigFactoryInterface $config_factory,
      EntityTypeManagerInterface $entity_type_manager,
      BoostCacheFileInterface $boost_file
    ) {
    $this->config = $config_factory->get('boost.settings');
    $this->entityTypeManager = $entity_type_manager;
    $this->boostCacheFile = $boost_file;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('boost.file')
    );
  }

  /**
   * Get XmlSitemap (Raw) Paths.
   */
  public function getXmlSitemapCrawlPaths() {
    $config = $this->config;
    $verbose = $config->get('verbose') ?? 0;
    if ($verbose == 1) {
      Timer::start('boost_retrieve_paths');
    }
    $path_arrays = [];
    $sitemaps = $config->get('xmlsitemap_crawl') ?? [];
    $basic_auth = $config->get('auth') ?? ['user' => '', 'pass' => ''];
    $guzzle = ['verify' => FALSE];
    if ($basic_auth['user'] != '' && $basic_auth['pass'] != '') {
      $guzzle['auth'] = [$basic_auth['user'], $basic_auth['pass']];
    }

    $guzzle['allow_redirects'] = TRUE;
    $client = new Client([
      'base_uri' => \Drupal::request()->getSchemeAndHttpHost(),
      'headers' => ['Accept' => 'application/xml'],
      'timeout' => 120,
      'request.options' => ['exceptions' => FALSE],
    ]);
    $promises = [];
    foreach ($sitemaps as $sitemap) {
      $promises[$sitemap] = $client->requestAsync("get", $sitemap, $guzzle);
    }
    $responses = Utils::settle($promises)->wait();
    foreach ($responses as $sitemap => $response) {
      if ($response['state'] === 'fulfilled') {
        $xml_object = simplexml_load_string($response['value']->getBody()->getContents());
        foreach ($xml_object->url as $mapurl) {
          $loc = (string) $mapurl->loc;
          $loc_parts = parse_url($loc);
          $loc_baseurl = $loc_parts['scheme'] . "://" . $loc_parts['host'];
          $path = str_replace($loc_baseurl, "", $loc);
          $path_arrays[$path] = [];
        }
      }
    }
    if ($verbose == 1) {
      $timer = Timer::read('boost_retrieve_paths') . 'ms';
      $placeholder = ['@timer' => $timer, '@count' => count($path_arrays)];
      Timer::stop('boost_retrieve_paths');
      \Drupal::messenger()->addMessage(t('Debug: Retrieved @count XML sitemap crawl paths in @timer', $placeholder));
    }
    return $path_arrays;
  }

  /**
   * Get Entity Paths.
   */
  public function getEntityPaths($args = []) {
    $config = $this->config;
    $verbose = $config->get('verbose') ?? 0;
    if ($verbose == 1) {
      Timer::start('boost_retrieve_paths');
    }
    $path_arrays = [];
    $entity_types_all = $this->entityTypeManager->getDefinitions();
    $entity_types = [];
    if (isset($args['entity_type'])) {
      $entity_types = [$args['entity_type'] => []];
      if (isset($args['entity_bundles'])) {
        $entity_types[$args['entity_type']] = $args['entity_bundles'];
      }
    }
    else {
      $entity_types = $config->get('path_entity_set') ?? [];
    }

    foreach ($entity_types as $entity_type => $entity_bundles) {
      if (array_key_exists($entity_type, $entity_types_all)) {
        $entity_type_definition = $entity_types_all[$entity_type];
        $entity_keys = $entity_type_definition->getKeys();
        $entity_type_link_templates = $entity_type_definition->getLinkTemplates();
        $canonical_path = $entity_type_link_templates['canonical'];
        $explode1 = explode('{', $canonical_path);
        $prefix = $explode1[0];
        $suffix = '';
        $explode2 = explode('}', $explode1[1]);
        if (isset($explode2[1])) {
          $suffix = $explode2[1];
        }
        $query = \Drupal::entityQuery($entity_type);
        $query->accessCheck(TRUE);
        if ($entity_bundles != []) {
          $query->condition($entity_keys['bundle'], $entity_bundles, 'IN');
        }
        $entity_ids = $query->execute();

        foreach ($entity_ids as $entity_id) {
          $entity_path = $prefix . $entity_id . $suffix;
          $properties = ['path' => $entity_path];
          $alias_object = $this->entityTypeManager->getStorage('path_alias')->loadByProperties($properties);
          if (!empty($alias_object)) {
            $path = reset($alias_object)->getAlias();
          }
          else {
            $path = $entity_path;
          }
          $path_arrays[$path] = [];
        }
      }
    }
    if ($verbose == 1) {
      $timer = Timer::read('boost_retrieve_paths') . 'ms';
      $placeholder = ['@timer' => $timer, '@count' => count($path_arrays)];
      Timer::stop('boost_retrieve_paths');
      \Drupal::messenger()->addMessage(t('Debug: Retrieved @count entity paths in @timer', $placeholder));
    }
    return $path_arrays;
  }

  /**
   * Get Aliases all Paths.
   */
  public function getAliasesAllPaths() {
    $config = $this->config;
    $verbose = $config->get('verbose') ?? 0;
    if ($verbose == 1) {
      Timer::start('boost_retrieve_paths');
    }
    $path_arrays = [];
    $aliases = $this->entityTypeManager->getStorage('path_alias')->loadByProperties(['status' => 1]);
    foreach ($aliases as $alias) {
      $path = $alias->get('alias')->isEmpty() ? $alias->get('path')->value : $alias->get('alias')->value;
      if ($path) {
        $path_arrays[$path] = [];
      }
    }
    if ($verbose == 1) {
      $timer = Timer::read('boost_retrieve_paths') . 'ms';
      $placeholder = ['@timer' => $timer, '@count' => count($path_arrays)];
      Timer::stop('boost_retrieve_paths');
      \Drupal::messenger()->addMessage(t('Debug: Retrieved @count alias paths in @timer', $placeholder));
    }
    return $path_arrays;
  }

  /**
   * Retrieve paths and trigger boost cache files generation.
   */
  public function generateCacheFiles($path_arrays = []) {
    $config = $this->config;
    $paths_unchecked = array_keys($path_arrays);
    $paths = [];
    // Check if cache files exist:
    foreach ($paths_unchecked as $path_unchecked) {
      $uri = $this->boostCacheFile->getUri($path_unchecked);
      if (!file_exists($uri)) {
        $paths[] = $path_unchecked;
      }
    }
    drupal_flush_all_caches();
    $base_uri = \Drupal::request()->getSchemeAndHttpHost();
    $basic_auth = $config->get('auth') ?? ['user' => '', 'pass' => ''];

    $guzzle = ['verify' => FALSE];
    if ($basic_auth['user'] != '' && $basic_auth['pass'] != '') {
      $guzzle['auth'] = [$basic_auth['user'], $basic_auth['pass']];
    }
    $chunk_size = $config->get('chunk_generate') ?? 24;
    $batch_builder = (new BatchBuilder())
      ->setTitle(t('Processing Batch'))
      ->setFinishCallback([$this, 'generateCacheFilesBatchFinished'])
      ->setInitMessage(t('Batch is starting'))
      ->setProgressMessage(t('Processed @current out of @total.'))
      ->setErrorMessage(t('Batch has encountered an error'));

    foreach (array_chunk($paths, $chunk_size) as $chunk) {
      $batch_builder->addOperation([
        $this,
        'generateCacheFilesBatchProcess'
],
          [$chunk, $base_uri, $guzzle]
      );
    }
    batch_set($batch_builder->toArray());
    return;
  }

  /**
   * Generates files in batches.
   */
  public static function generateCacheFilesBatchProcess($paths, $base_uri, $guzzle, &$context) {
    $client = new Client([
      'base_uri' => $base_uri,
      'headers' => ['X-Boost-Cache' => 'purge'],
    ]);
    $context['message'] = 'Trigger: ' . count($paths) . ' URIs via get.';
    $promises = [];
    foreach ($paths as $path) {
      $promises[$path] = $client->requestAsync("get", $path, $guzzle);
    }
    $responses = Utils::settle($promises)->wait();
    foreach ($responses as $path => $response) {
      if ($response['state'] === 'fulfilled') {
        $status = $response['value']->getStatusCode();
        $context['results'][$status][$path] = $response;
      }
    }
  }

  /**
   * Batch process callback after completion.
   *
   * @param bool $success
   *   TRUE if batch successfully completed.
   * @param array $results
   *   Batch results.
   * @param array $operations
   *   Operations.
   */
  public function generateCacheFilesBatchFinished($success = FALSE, array $results = [], array $operations = []) {
    $message = \Drupal::translation()->translate('Finished with an error.');
    if ($success) {
      $response_200 = 0;
      if (isset($results[200])) {
        $response_200 = count($results[200]);
      }
      $message = \Drupal::translation()->formatPlural(
          $response_200, 'One status 200 response.', '@count status 200 responses.'
      );
    }
    \Drupal::messenger()->addMessage($message);
  }

}
