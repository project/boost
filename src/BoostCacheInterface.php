<?php

declare(strict_types=1);

namespace Drupal\boost;

/**
 * BoostCache Interface.
 */
interface BoostCacheInterface {

  /**
   * Indexes a BoostCache file.
   *
   * @param string $response
   *   The response body.
   */
  public function index(string $response);

  /**
   * Retrieves a BoostCache file.
   *
   * @return string
   *   Returns the file content.
   */
  public function retrieve(): string;

  /**
   * Deletes an individual cache file.
   *
   * @return bool
   *   Returns FALSE if the file doesn't exist.
   */
  public function delete(): bool;

}
